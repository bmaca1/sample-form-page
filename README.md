# Mickeys Chicago Style Form

## Getting Started

1. **Download the HTML File:**

   - Download the `mickeys_form.html` file from this repo.

2. **Customize the Form (Optional):**

   - If you want to customize the form, feel free to modify the HTML and CSS styles. You should modify the email that is hardcoded in the HTML to yours.

3. **Run on Local Browser:**
   - Right-click on the `mickeys_form.html` file.
   - Open with your preferred web browser.

### Demo

![til](./demo/mickeys.gif)
